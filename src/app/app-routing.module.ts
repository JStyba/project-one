import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { FunResolver } from './fun.resolver';
import { HomeComponent } from './home/home.component';
import { HooksComponent } from './hooks/hooks.component';
import { LockGuard } from './lock.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PlaygroundComponent } from './playground/playground.component';
import { TablesComponent } from './tables/tables.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
    // redirectTo: 'home'
  },
  {
    path: 'home',
    component: HomeComponent,
    
  },
  {
    path: 'hooks',
    component: HooksComponent,
    canActivate: [LockGuard]
  },
  {
    path: 'tables',
    component: TablesComponent,
  },
  {
    path: 'form',
    component: FormComponent,
  },
  {
    path: 'playground',
    component: PlaygroundComponent,
    resolve: { pic: FunResolver}

  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
