import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sda-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent {

  moviesArray = [
    {
      name: 'Robin Hood',
      department: '',
      manager: null,
      salary: 200
    },
    {
      name: 'Arsene Wenger',
      department: 'Bar',
      manager: 'Friar Tuck',
      salary: 50
    },
    {
      name: 'Friar Tuck',
      department: 'Foo',
      manager: 'Robin Hood',
      salary: 100
    },
    {
      name: 'Little John',
      department: 'Foo',
      manager: 'Robin Hood',
      salary: 100
    },
    {
      name: 'Sam Allardyce',
      department: '',
      manager: null,
      salary: 250
    },
    {
      name: 'Dimi Berbatov',
      department: 'Foo',
      manager: 'Little John',
      salary: 50
    }
  ]

  moviesObject = {
    one: {
      name: 'Robin Hood',
      department: '',
      manager: null,
      salary: 200
    },
    two: {
      name: 'Arsene Wenger',
      department: 'Bar',
      manager: 'Friar Tuck',
      salary: 50
    },
    three: {
      name: 'Friar Tuck',
      department: 'Foo',
      manager: 'Robin Hood',
      salary: 100
    },
    four: {
      name: 'Little John',
      department: 'Foo',
      manager: 'Robin Hood',
      salary: 100
    },
    five: {
      name: 'Sam Allardyce',
      department: '',
      manager: null,
      salary: 250
    },
    six: {
      name: 'Dimi Berbatov',
      department: 'Foo',
      manager: 'Little John',
      salary: 50
    }
}

public stringVariable: string = 'Hello World';
  numberVariable: number = 42;
  booleanVariable: boolean = true;
  arrayOfStringI: string[] = ['Hello', 'World'];
  arrayOfStringII: Array<string> = ['There', 'You', 'Go'];
  variableAsNull: null | string = null;
  differentStringInit: string = `Hello World! ${this.numberVariable} great huh?`; // remember to use backtick
  variableAsUndefined: undefined | string = undefined;
  variableAsAny: any = 'Hello Again';
  arrayOfUnknown: Array<unknown> = [1, 'Hello', true];
  variableAsNever: never;
  objectVariableI: object = {name: 'Jarod', age: 42};
  objectVariableII: {name: string, age: number} = {name: 'Morty', age: 9001};
  objectVariableIII: {[key: string]: string | number} = {name: 'Peter', secondName: 'Bob', age: 12};
  objectVariableIV: IObjectType = {name: 'Peter', secondName: 'Bob', age: 12};
  private privateVariable: string = 'private';
  functionVariable: number = this.addNumbers(1, 2);
  person: Person = new Person('Adam', 90);
  directions: Directions = Directions.Down
  genClassVariable: GenClass<Class1> = new GenClass();


  addNumbers(a: number, b: number): number {
    return a + b;
  }


  functionNotDoingMuch(): void {
    let a: number = 7;
    let b: number = 3;
    const result = 0;
    // result = a + b; won't work because result is a const
    console.log('type of result variable: ', typeof result);
    throw new Error('a very bad thing happened');
  }

  loopFunction(array: unknown[]): Array<unknown> {
    let a = [];
    // for (let i = 0; i < array.length; i++) {
    //   console.log(array[i]);
    //   a.push(array[i]);
    // }
    array.forEach((e: unknown):void => { // exactly the same as ordinary for loop
      a.push(e);
    })
    return a;
  }

}

interface IObjectType {
  name: string;
  secondName: string;
  age: number;
}

class Person {
  constructor(public name: string, public age: number) {
    this.name = name;
    this.age = age;
  }
}

enum Directions {
  Up = 'North',
  Down = 'South',
  Left = 'West',
  Right = 'East'
}

class Class1 {}
class Class2 {}

class GenClass<T> {
  content: T;
}
