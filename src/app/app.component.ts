import { Component } from '@angular/core';
import { StateService } from './state.service';

@Component({
  selector: 'sda-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  name: string = 'Jarek Doe';
 
  stateFromService = '';
  title = 'awesome project';

  constructor(private stateService: StateService) {
  }

  ngOnInit(): void {
    console.log(this.stateService.sharedValue)
  }
  
  

  childHasSpoken(text: string): void {
    console.log(text);
  }
}


