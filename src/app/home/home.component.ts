import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import {take} from 'rxjs/operators';
import { StateService } from '../state.service';

@Component({
  selector: 'sda-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public stateService: StateService) { }

  ngOnInit(): void {
  }

  generateToken() {
    this.stateService.token = 7;
    const number = interval(1000);
    const takeNumbers = number.pipe(take(10));
    takeNumbers.subscribe(x => {
      if (this.stateService.token > 0 ) {
        this.stateService.token -= 1;
      }
    })
  }
}
