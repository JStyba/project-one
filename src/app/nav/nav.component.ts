import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StateService } from '../state.service';

@Component({
  selector: 'sda-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
@Input() myName: string = 'Name placeholder';
@Output() myEvent = new EventEmitter<string>();
activeComponent: number = 0;

constructor(private stateService: StateService) {
  this.stateService.sharedValue = 'set from nav component';

};

  ngOnInit(): void {
  }

  sendEvent(text: string): void {
    this.myEvent.emit(text);
  }

  setActiveComponent(num: number): void {
    this.activeComponent = num;
  }

}
